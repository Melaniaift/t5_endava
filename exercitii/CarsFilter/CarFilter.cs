﻿using System.Collections.Generic;
using System.Linq;
using CarsFilter.Enums;

namespace CarsFilter
{
    public class CarFilter
    {
        public List<Car> FilterByType(IEnumerable<Car> cars, CarType type) =>
            cars.Where(c => c.Type == type).ToList();

        public List<Car> FilterByColor(IEnumerable<Car> cars, Color Color) =>
            cars.Where(c => c.Color == Color).ToList();

        public List<Car> FilterByTypeColor(IEnumerable<Car> cars, CarType type, Color Color) =>
            cars.Where(c => ( c.Color == Color) && (c.Type == type)).ToList();
    }
}
