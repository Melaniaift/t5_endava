﻿using System;

namespace UserCreation
{
    class Program
    {
        public static void Writee(string s) {
            s = "Enter your " + s + " name:";
            Console.WriteLine(s);
        }

        static void Main(string[] args)
        {
            var user = new Person();
            Console.WriteLine("Welcome to user creation app!");

            while ((user.FirstName == "")|| (user.FirstName == null))
            {
                Writee("first");
                user.FirstName = Console.ReadLine();
            }

            while ((user.LastName == "") || (user.LastName == null))
            {
                Writee("last");
                user.LastName = Console.ReadLine();
            }

            Console.WriteLine($"Your new username is {user.FirstName.Substring(0, 1)}{user.LastName}");
        }
    }
}
